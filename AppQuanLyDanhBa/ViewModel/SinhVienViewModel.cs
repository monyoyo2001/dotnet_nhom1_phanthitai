﻿using AppQuanLyDanhBa.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AppQuanLyDanhBa.ViewModel
{
    public enum KetQua
    {
        TrungMa,
        ThanhCong,
    }
    public class SinhVienViewModel
    {
        public int MaSinhVien { get; set; }
        public string TenSinhVien { get; set; }

        public string Email { get; set; }

        public int? SoDienThoai { get; set; }

        public string DiaChi { get; set; }

        public int? MaNhom { get; set; }

    }
}
