﻿using AppQuanLyDanhBa.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppQuanLyDanhBa.ViewModel
{
    public enum KetQuaNhomLienLac
    {
        TrungMa,
        ThanhCong,
    }
    public class NhomLienLacViewModel
    {
        public int MaNhom { get; set; }

        public String TenNhom { get; set; }

       

    }
}
