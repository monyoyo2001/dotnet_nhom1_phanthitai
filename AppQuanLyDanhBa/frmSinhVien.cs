﻿using AppQuanLyDanhBa.Model;
using AppQuanLyDanhBa.Services;
using AppQuanLyDanhBa.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppQuanLyDanhBa
{
    public partial class frmSinhVien : Form
    {
        public frmSinhVien()
        {
            InitializeComponent();
            NapDsNhomLienLac();

        }

        void NapDsNhomLienLac()
        {
            var ls = NhomLienLacService.GetList();
            cbbNhomLienLac.DataSource = ls;
            cbbNhomLienLac.ValueMember = "MaNhom";
            cbbNhomLienLac.DisplayMember = "TenNhom";
        }

        public NhomLienLacViewModel selectedNhom
        {
            get
            {
                return cbbNhomLienLac.SelectedItem as NhomLienLacViewModel;
            }
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            var sv = new SinhVien
            {
                MaSinhVien = int.Parse(txtMaSinhVien.Text),
                TenSinhVien = txtTenSinhVien.Text,
                Email = txtEmail.Text,
                SoDienThoai = int.Parse(txtSoDienThoai.Text),
                DiaChi = txtDiaChi.Text,
                MaNhom = selectedNhom.MaNhom,
            };
            if (SinhVienService.AddSinhVien(sv) == KetQua.ThanhCong)
            {
                //Để báo kết quả mình mới thêm vô
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Mã sinh viên bị trùng", "Thông Báo");
                txtMaSinhVien.Focus();
            }
        }
    }
}
