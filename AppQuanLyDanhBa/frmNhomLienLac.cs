﻿using AppQuanLyDanhBa.Model;
using AppQuanLyDanhBa.Services;
using AppQuanLyDanhBa.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppQuanLyDanhBa
{
    public partial class frmNhomLienLac : Form
    {
        public frmNhomLienLac()
        {
            InitializeComponent();
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            var lh = new NhomLienLac
            {
                TenNhom = txtTenNhom.Text
                  
            };
            if (NhomLienLacService.AddNhom(lh) == KetQuaNhomLienLac.ThanhCong)
            {
                DialogResult = DialogResult.OK;
            }
        }
    }
}
