﻿using AppQuanLyDanhBa.Services;
using AppQuanLyDanhBa.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppQuanLyDanhBa
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            NapDsNhomLienLac();
            NapDsSinhVien();
        }
        
        void NapDsNhomLienLac()
        {
            var ls = NhomLienLacService.GetList();
            nhomLienLacViewModelBindingSource.DataSource = ls;
            girdNhomLienLac.DataSource = nhomLienLacViewModelBindingSource;
        }

        public NhomLienLacViewModel selectedNhomLienLac
        {
            get
            {
                return nhomLienLacViewModelBindingSource.Current as NhomLienLacViewModel;
            }
        }

        public SinhVienViewModel selectedSinhVien
        {
            get
            {
                return sinhVienViewModelBindingSource.Current as SinhVienViewModel;
                
            }
           
        }

        void NapDsSinhVien()
        {
            if (selectedNhomLienLac != null)
            {
                var ls = SinhVienService.GetList(selectedNhomLienLac.MaNhom);
                sinhVienViewModelBindingSource.DataSource = ls;
                girdSinhVien.DataSource = sinhVienViewModelBindingSource;
            }            
        }

        void NapThongTin()
        {
            if(selectedSinhVien !=null)
            {
               
                txtTen.Text = selectedSinhVien.TenSinhVien;
                txtEmail.Text = selectedSinhVien.Email;
                txtSDT.Text = selectedSinhVien.SoDienThoai.ToString();
                txtDiaChi.Text = selectedSinhVien.DiaChi; ;
            }
        }

        private void girdNhomLienLac_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           if (selectedNhomLienLac != null)
            {
                NapDsSinhVien();

            }
            
        }

        private void girdSinhVien_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            NapThongTin();
        }

        private void btnThemLienLac_Click(object sender, EventArgs e)
        {
            var f = new frmSinhVien();
            if(f.ShowDialog() == DialogResult.OK)
            {
                NapDsSinhVien();
            }
        }

        private void btnXoaLienLac_Click(object sender, EventArgs e)
        {
            if (selectedSinhVien != null)
            {
                var rs = MessageBox.Show("Bạn có chắc là muốn xóa ?", "Chú ý", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (rs == DialogResult.OK)
                {
                    SinhVienService.DeleteSinhVien(selectedSinhVien);
                    NapDsSinhVien();
                }
            }
        }

        private void btnThemNhom_Click(object sender, EventArgs e)
        {
            var f = new frmNhomLienLac();
            if (f.ShowDialog() == DialogResult.OK)
            {
                NapDsNhomLienLac();
                NapDsSinhVien();
            }
        }

        private void btnXoaNhom_Click(object sender, EventArgs e)
        {
            if (selectedNhomLienLac != null || selectedSinhVien != null)
            {
                var rs = MessageBox.Show("Bạn có chắc là muốn xóa ?", "Chú ý", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (rs == DialogResult.OK)
                {
                    NhomLienLacService.DeleteNhom(selectedNhomLienLac);
                    NapDsNhomLienLac();
                    NapDsSinhVien();
                };
            }
        }

        private void txtTimkiem_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if (selectedNhomLienLac != null)
                {
                    var ls = SinhVienService.SearchSinhVien(selectedNhomLienLac.MaNhom, txtTimKiem.Text);
                    sinhVienViewModelBindingSource.DataSource = ls;
                    girdSinhVien.DataSource = sinhVienViewModelBindingSource;
                }
            }
        }
    }
}
