﻿using AppQuanLyDanhBa.Model;
using AppQuanLyDanhBa.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppQuanLyDanhBa.Services
{
    public class NhomLienLacService
    {

        /// <summary>
        /// Hiển thị nhóm liên lạc
        /// </summary>
        /// <returns></returns>
        public static List<NhomLienLacViewModel> GetList()
        {
            var db = new AppDBContext();
            var rs = db.NhomLienLacs.Select(e => new NhomLienLacViewModel
            {
                MaNhom = e.MaNhom,
                TenNhom = e.TenNhom,
            }).ToList();

            return rs;
        }


        /// <summary>
        /// Thêm một nhóm liên lạc
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static KetQuaNhomLienLac AddNhom(NhomLienLac n)
        {
            var db = new AppDBContext();
            int count = db.NhomLienLacs.Where(e => e.MaNhom == n.MaNhom).Count();
            if (count > 0)
            {
                return KetQuaNhomLienLac.TrungMa;
            }
            else
            {
                db.NhomLienLacs.Add(n);

                db.SaveChanges();

                return KetQuaNhomLienLac.ThanhCong;
            }

        }


        /// <summary>
        /// Xóa nhóm liên lạc
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static KetQuaNhomLienLac DeleteNhom(NhomLienLacViewModel n)
        {
            var db = new AppDBContext();

            var nhomLienLac = db.NhomLienLacs.Where(e => e.MaNhom == n.MaNhom).FirstOrDefault();
            var sinhVien = db.SinhViens.Where(e => e.MaNhom == nhomLienLac.MaNhom).ToList();
            if (sinhVien == null)
            {
                db.NhomLienLacs.Remove(nhomLienLac);
            }
            else
            {
                foreach (var s in sinhVien)
                {
                    db.SinhViens.Remove(s);
                }
                db.NhomLienLacs.Remove(nhomLienLac);
            }

            db.SaveChanges();
            return KetQuaNhomLienLac.ThanhCong;
        }
    }
}
