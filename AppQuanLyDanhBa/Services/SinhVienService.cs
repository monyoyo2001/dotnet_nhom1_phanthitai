﻿using AppQuanLyDanhBa.Model;
using AppQuanLyDanhBa.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppQuanLyDanhBa.Services
{
    public class SinhVienService
    {

        /// <summary>
        /// Hiển thị toàn bộ thông tin liên lạc của sinh viên dựa vào nhóm liên lạc
        /// </summary>
        /// <param name="idNhom"></param>
        /// <returns></returns>
        public static List<SinhVienViewModel> GetList(int idNhom)
        {
            var db = new AppDBContext();
            var rs = db.SinhViens.Where(e => e.MaNhom == idNhom).Select(e => new SinhVienViewModel
            {
                MaSinhVien = e.MaSinhVien,
                TenSinhVien = e.TenSinhVien,
                Email = e.Email,
                SoDienThoai = e.SoDienThoai,
                DiaChi = e.DiaChi,
                MaNhom = e.MaNhom,
            }).ToList();

            return rs;
        }


        /// <summary>
        /// Thêm một thông tin liên lạc của sinh viên
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        public static KetQua AddSinhVien(SinhVien sv)
        {
            var db = new AppDBContext();
            int count = db.SinhViens.Where(e => e.MaSinhVien == sv.MaSinhVien).Count();
            if (count > 0)
            {
                return KetQua.TrungMa;
            }
            else
            {
                db.SinhViens.Add(sv);
                db.SaveChanges();
                return KetQua.ThanhCong;
            }
        }


        /// <summary>
        /// Xóa một thông tin liên lạc của sinh viên
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        public static KetQua DeleteSinhVien(SinhVienViewModel sv)
        {

            var db = new AppDBContext();
            var sinhVien = db.SinhViens.Where(e => e.MaSinhVien == sv.MaSinhVien).FirstOrDefault();
            db.SinhViens.Remove(sinhVien);

            db.SaveChanges();
            return KetQua.ThanhCong;
        }

        
        /// <summary>
        /// Tìm kiếm thông tin liên lạc của sinh viên
        /// </summary>
        /// <param name="maNhom"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public static List<SinhVienViewModel> SearchSinhVien(int maNhom , String search)
        {
            var rs = GetList(maNhom);
            var ds = rs.Where(e => e.TenSinhVien.ToLower().Contains(search.ToLower()));
            return ds.ToList();
        }

    }
}
